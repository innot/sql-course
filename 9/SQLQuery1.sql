create trigger StageLaps
	on Stage after update as
		declare @new_length as real;
		declare @stage_id as int;

		select @new_length = s.lap_length, @stage_id = s.stage_id
		from Stage s join inserted i on s.stage_id = i.stage_id
		where s.lap_length != i.lap_length
			and (s.date is null or s.date > GETDATE())

		if @stage_id is not null
			update Stage
			set total_laps = ROUND(300.0/lap_length, 0);