select * from stage;

SET IDENTITY_INSERT [Stage] ON;
INSERT INTO [Stage] ([stage_id], [date], [audience], [city_id], [title], [lap_length], [total_laps]) 
		VALUES (16, convert(date,'2020-06-16', 102), 1000, 1, 'Temp stage', 100, 12);
SET IDENTITY_INSERT [Stage] OFF;

update Stage
	set lap_length = 110
	where stage_id = 16;

select * from stage;

update Stage
	set lap_length = 110
	where stage_id = 1;

select * from stage;