-- repeatable read and phantom read
set tran isolation level repeatable read;
begin tran;




update Results
	set place_taken = 1
	where stage_id = 6 
		and pilot_id = 2;	
commit;