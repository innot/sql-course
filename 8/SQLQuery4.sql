CREATE TABLE test.Tmp(
	[country_id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
CONSTRAINT [PK_Country_1] PRIMARY KEY 
(
	[country_id] ASC
)
);

CREATE TABLE test.T2(
	[manufacturer_id] [smallint] IDENTITY(1,1) NOT NULL,
	[title] [varchar](50) NOT NULL,
	[country_id] [smallint] NOT NULL
);

ALTER TABLE test.T2  
	ADD CONSTRAINT [FK_T2_Tmp] FOREIGN KEY([country_id])
		REFERENCES test.Tmp ([country_id]);
		