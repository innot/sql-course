if exists(
	select * from sys.views
	where name = 'EngineTyresStage' and schema_id = schema_id('dbo')
	)
drop view EngineTyresStage
go

create view dbo.EngineTyresStage as (
	select engine.title as engine, tyres.title as tyres, stage.title as stage, (
			select count(*)
			from results inner join car on car.car_id = results.car_id
			where results.stage_id = stage.stage_id and
				car.engine_id = engine.engine_id and 
				exists (select *
						from ResultTyres
						where ResultTyres.tyres_id = tyres.tyres_id and
							results.pilot_id = ResultTyres.pilot_id and 
							results.stage_id = ResultTyres.stage_id
					) and
				(results.place_taken = 1 or results.place_taken = 2 or results.place_taken = 3 or
					results.place_taken = 4 or results.place_taken = 5 or results.place_taken = 6)
		) as podiums
	from engine cross join tyres
				cross join stage
	group by engine.title, engine.engine_id, tyres.title, tyres.tyres_id, stage.title, stage.stage_id
)
go

select * from EngineTyresStage