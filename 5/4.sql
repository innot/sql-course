if exists (
	select * from sys.views
	where name = 'TeamSeasonPlaces' and schema_id = schema_id('dbo')
)
drop view TeamSeasonPlaces
go

create view dbo.TeamSeasonPlaces as (
	select t.title as team, sc.place_taken as place, (
		select count(*)
		from stage st inner join results r on st.stage_id = r.stage_id
		where t.team_id = r.team_id and
			  r.place_taken = sc.place_taken and
			  (datediff(day, st.date, getdate()) < 365) and ((
			  	select count(*) as x
			  	from laps l
			  	where l.stage_id = r.stage_id and
			  		  l.pilot_id = r.pilot_id
			  ) >= st.total_laps)

		
	) as times_taken
	from team t, scores sc
	group by t.title, t.team_id, sc.place_taken
)
go

select * from TeamSeasonPlaces