use [Formula 1]
select * into ResultsBig from Results
select * into ResultsBigIndex from Results
go

---------------------

INSERT INTO [ResultsBig] 
	select stage_id, pilot_id, team_id,	total_time,	place_taken, car_id
	from Results
	
INSERT INTO [ResultsBigIndex] 
	select stage_id, pilot_id, team_id,	total_time,	place_taken, car_id
	from Results;
	
go 10000

--------------------
set statistics time on
set statistics io on
go

dbcc dropcleanbuffers
go
select stage_id, pilot_id, team_id,	total_time,	place_taken, car_id
	from [ResultsBig]
	where stage_id = 1 and team_id = 5

-- (����� ����������: 10001)
-- ������� "ResultsBig". ����� ���������� 1, ���������� ������ 2734, ���������� ������ 33, ����������� ������ 2743, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.

--  ����� ������ SQL Server:
--    ����� �� = 63 ��, ����������� ����� = 1202 ��.

-------------------------

create nonclustered index [IX_stage_id] on dbo.ResultsBigIndex (
	stage_id asc,
	team_id asc
)

dbcc dropcleanbuffers
go
select stage_id, pilot_id, team_id,	total_time,	place_taken, car_id
	from [ResultsBigIndex]
	where stage_id = 1 and team_id = 5

-- (����� ����������: 10001)
-- ������� "ResultsBigIndex". ����� ���������� 1, ���������� ������ 2734, ���������� ������ 84, ����������� ������ 2741, lob ���������� ������ 0, lob ���������� ������ 0, lob ����������� ������ 0.

--  ����� ������ SQL Server:
--    ����� �� = 47 ��, ����������� ����� = 1155 ��.
-------------------------

dbcc dropcleanbuffers
go

select stage_id, pilot_id, team_id,	total_time,	place_taken, car_id
	from [ResultsBig]
	where stage_id = 1 and team_id = 5

select stage_id, pilot_id, team_id,	total_time,	place_taken, car_id
	from [ResultsBigIndex]
	where stage_id = 1 and team_id = 5

go

-- 50/50