SELECT SUM(item.total - price.min_price * item.quantity) as result
FROM ITEM, PRODUCT, PRICE
WHERE item.product_id = product.product_id AND
	product.description = 'SP TENNIS RACKET' AND
	price.product_id = product.product_id AND
	item.actual_price > price.min_price