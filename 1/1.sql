SELECT salary, first_name, last_name, [function] AS job
	FROM EMPLOYEE, JOB
	WHERE salary >= ALL (
		SELECT employee1.salary
			FROM employee employee1
			WHERE employee.job_id = employee1.job_id
		) AND employee.job_id = job.job_id
ORDER BY salary DESC