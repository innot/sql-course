/*manufacturers producing tyres and engines; 
teams and stages in which those teams
participated using tyres and engines of selected manufacturer*/

select distinct manufacturer.manufacturer_id, results.stage_id, results.team_id
from manufacturer inner join engine on manufacturer.manufacturer_id = engine.manufacturer_id
				  inner join tyres on manufacturer.manufacturer_id = tyres.manufacturer_id
				  inner join resulttyres on resulttyres.tyres_id = tyres.tyres_id
				  inner join results on results.stage_id = resulttyres.stage_id
				  inner join car on car.engine_id = engine.engine_id and car.car_id = results.car_id
where results.team_id = car.team_id