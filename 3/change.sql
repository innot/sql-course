--alter table	[PilotTeam] 
--	drop constraint [PK_PilotTeam];

ALTER TABLE PilotTeam ADD t_field bigint;
UPDATE PilotTeam SET t_field = varcharfield;
ALTER TABLE table_name DROP COLUMN varcharfield;
ALTER TABLE table_name RENAME COLUMN clobfield TO varcharfield;

--alter table [PilotTeam] nocheck constraint [DF__PilotTeam__contr__5006DFF2]
--alter table [PilotTeam]
--	add constraint DF_PilotTeam_cstart DEFAULT getdate() - convert(date, '0-1-0', 102) FOR contract_start;