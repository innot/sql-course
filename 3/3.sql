select stage_id
from Stage
group by stage_id
having (
	select SUM(scores.score)
	from results inner join resulttyres on results.stage_id = resulttyres.stage_id and
										   results.pilot_id = resulttyres.pilot_id
				 inner join tyres on ResultTyres.tyres_id = tyres.tyres_id
				 inner join manufacturer on tyres.manufacturer_id = manufacturer.manufacturer_id
				 inner join scores on results.place_taken = scores.place_taken
	where ResultTyres.start_time = convert(time, '00:00:00', 108) and 
		manufacturer.title = 'Michelin' and
		results.stage_id = Stage.stage_id
) > (
	select SUM(scores.score)
	from results inner join resulttyres on results.stage_id = resulttyres.stage_id and
										   results.pilot_id = resulttyres.pilot_id
				 inner join tyres on ResultTyres.tyres_id = tyres.tyres_id
				 inner join manufacturer on tyres.manufacturer_id = manufacturer.manufacturer_id
				 inner join scores on results.place_taken = scores.place_taken
	where ResultTyres.start_time = convert(time, '00:00:00', 108) and 
		manufacturer.title != 'Michelin' and
		results.stage_id = Stage.stage_id
)