select Results.pilot_id, AVG(laps*Stage.lap_length/(
	(DATEPART(HOUR, Results.total_time) * 3600) +
	(DATEPART(MINUTE, Results.total_time) * 60) +
	(DATEPART(SECOND, Results.total_time))
	)) as speed
from Results inner join Stage on stage.stage_id = results.stage_id 
		   inner join (
				select Results.pilot_id as pilot_id, Results.stage_id as stage_id, COUNT(lap_n) as laps
				from Results inner join Laps on Results.pilot_id = Laps.pilot_id 
											and Results.stage_id = Laps.stage_id
							 inner join Stage on stage.stage_id = results.stage_id 
				group by Results.pilot_id, Results.stage_id
			) t1 on t1.pilot_id = results.pilot_id and t1.stage_id = results.stage_id
where datediff(day, stage.date, getdate()) > 365 and 
	 Results.total_time != '00:00:00.000'
group by Results.pilot_id