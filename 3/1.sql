select results.pilot_id, count(stage_id) as stages, sum(CASE
        WHEN results.place_taken = 1 THEN 10
        WHEN results.place_taken = 2 THEN 8 
        WHEN results.place_taken = 3 THEN 6  
        WHEN results.place_taken = 4 THEN 5 
        WHEN results.place_taken = 5 THEN 4
        WHEN results.place_taken = 6 THEN 3
        WHEN results.place_taken = 7 THEN 2
        WHEN results.place_taken = 8 THEN 1
        ELSE 0
        END) as total
from results inner join pilot on pilot.pilot_id = results.pilot_id
group by results.pilot_id
order by total desc