select team_id, substring(convert(varchar, round(avg(finished*100.0/total), 2)), 1, 5) + '%' as result
from (
	Select Results.team_id, Results.pilot_id, Results.stage_id, COUNT(lap_n) as finished, stage.total_laps as total
	from Results inner join Laps on laps.pilot_id = Results.pilot_id and
									laps.stage_id = Results.stage_id
				 inner join stage on stage.stage_id = Results.stage_id
				 inner join team on team.team_id = Results.team_id
	group by Results.team_id, Results.pilot_id, Results.stage_id, stage.total_laps
	) t1
group by team_id